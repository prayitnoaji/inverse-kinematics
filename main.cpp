#include "mbed.h"
#include "stdio.h"
#include "arm_math.h"
#include "TextLCD/TextLCD.cpp"

TextLCD lcd(PC_5,PA_12,PB_12,PB_2,PB_15,PC_4); /*PA_14 && PA_13 JANGAN DIGUNAKAN!!!!!!  BUAT JALAN DOWNLOADER*/

//kakiA
PwmOut kide1(PA_9);
PwmOut kide2(PC_7);
PwmOut kide3(PB_6); //semakin besar masuk
//kakiB
PwmOut kade1(PA_7); //semakin besar mundur
PwmOut kade2(PA_6); //semakin besar turun
PwmOut kade3(PA_5); //semakin besar keluar
//kakiC
PwmOut kabe1(PC_8);  // semakin besar mundur   //PA10
PwmOut kabe2(PB_3);  //semakin besar atas
PwmOut kabe3(PA_11);  //semakin besar masuk   //PB5
//kakiD
PwmOut kibe1(PB_7);  //semakin besar maju   //PB4
PwmOut kibe2(PB_10); //semakin besar turun
PwmOut kibe3(PB_1);   //semakin besar keluar   //PA8

DigitalIn summy(PA_8);
DigitalIn summy1(PA_10);
DigitalIn summy2(PB_5);
DigitalIn summy3(PB_4);

DigitalIn usBtn(USER_BUTTON);
DigitalOut buzz(PC_9);

//-----------------------------------------------------------------------------------------------selector
DigitalOut s0(PC_10);
DigitalOut s1(PC_11);
DigitalOut s2(PD_2);

//Timer Interrupt
Ticker Kinematics;
Ticker msInterrupt;

//--------------------------------------------------------------------------------------------GLOBAL VARIABLE
int penandaBuzzer;

//Stepping
unsigned int stepstats, langkah, inc, servo, step, speed=55, speedrotasi=50, stepWidth=40, stepWidthRotasi=10, stepWidthApi=30;

//Direct Kinematics Input;
float coxaLength=50, femurLength=35, tibiaLength=85;
float sideLength=45;
float sq(float nilai){float out=(nilai*nilai); return out;}//end akar kuadrat
float posX[4], posY[4], posZ[4], pX[4], pZ[4], pY[4];
float gaitX[4], gaitY[4], gaitZ[4], turn[4];
float bodyCoxaOffset=sideLength;
float bodyCoxaOffsetX=sideLength/2;
float bodyCoxaOffsetZ=sqrt(sq(sideLength)-sq(bodyCoxaOffsetX));
static double bodyCoxaCenterOffsetX[4], bodyCoxaCenterOffsetZ[4],feetPosX[4],feetPosY[4],feetPosZ[4];
double transformX, transformY, transformZ, coxaFeetDistance, IKSW, IKA1,IKA2,Tangle;
double newPosX[4], newPosZ[4], newPosY[4], tibiaAngle[4], femurAngle[4], coxaAngle[4];
float kakiA[3], kakiB[3], kakiC[3], kakiD[3];
float tujuanZ[4], tujuanY[4], tujuanX[4], tujuanTurn[4];
//static double totalX, totalZ, totalY, sinRotX, sinRotY, sinRotZ, cosRotX, cosRotY, cosRotZ, bodyIKX, bodyIKZ, bodyIKY;

//----------------------------------------------------------------------------------------------------------------------------single leg Movement


void kakia(float offsetx, float offsety, float offsetz)
{
    kade1.pulsewidth_us(offsetx);
    kade2.pulsewidth_us(offsety);
    kade3.pulsewidth_us(offsetz);
}

void kakib(float offsetx, float offsety, float offsetz)  //kakic(0,-120,-250)
{
    kabe1.pulsewidth_us(offsetx);
    kabe2.pulsewidth_us(offsety);
    kabe3.pulsewidth_us(offsetz);
}

void kakic(float offsetx, float offsety, float offsetz)
{
    kibe1.pulsewidth_us(offsetx);
    kibe2.pulsewidth_us(offsety);
    kibe3.pulsewidth_us(offsetz);
}

void kakid(float offsetx, float offsety, float offsetz)
{
    kide1.pulsewidth_us(offsetx);
    kide2.pulsewidth_us(offsety);
    kide3.pulsewidth_us(offsetz);
}

void servoOFF(){

	    kide1.pulsewidth_us(0);
	    kide2.pulsewidth_us(0);
	    kide3.pulsewidth_us(0);
	    kade1.pulsewidth_us(0);
	    kade2.pulsewidth_us(0);
	    kade3.pulsewidth_us(0);
	    kabe1.pulsewidth_us(0);
	    kabe2.pulsewidth_us(0);
	    kabe3.pulsewidth_us(0);
	    kibe1.pulsewidth_us(0);
	    kibe2.pulsewidth_us(0);
	    kibe3.pulsewidth_us(0);
}

//----------------------------------------------------------------------------------------------------------------------------START KINEMATICS
void IKinit(){

	bodyCoxaCenterOffsetX[0]=-bodyCoxaOffsetX; bodyCoxaCenterOffsetX[1]=bodyCoxaOffsetX; bodyCoxaCenterOffsetX[2]=-bodyCoxaOffsetX; bodyCoxaCenterOffsetX[3]=-bodyCoxaOffsetX;
	bodyCoxaCenterOffsetZ[0]=bodyCoxaOffsetZ; bodyCoxaCenterOffsetZ[1]=-bodyCoxaOffsetZ; bodyCoxaCenterOffsetZ[2]=-bodyCoxaOffsetZ; bodyCoxaCenterOffsetZ[3]=bodyCoxaOffsetZ;
	feetPosX[0]=cos(45*3.141592/180)*(coxaLength+femurLength); 	feetPosX[1]=cos(45*3.141592/180)*(coxaLength+femurLength); 			feetPosX[2]=-cos(45*3.141592/180)*(coxaLength+femurLength); 	feetPosX[3]=-cos(45*3.141592/180)*(coxaLength+femurLength);
	feetPosY[0]=tibiaLength; 									feetPosY[1]=tibiaLength; 											feetPosY[2]=tibiaLength; 										feetPosY[3]=tibiaLength;
	feetPosZ[0]=sin(45*3.141592/180)*(coxaLength+femurLength); 	feetPosZ[1]=-sin(45*3.141592/180)*(coxaLength+femurLength); 		feetPosZ[2]=-sin(45*3.141592/180)*(coxaLength+femurLength); 	feetPosZ[3]=sin(45*3.141592/180)*(coxaLength+femurLength);

//	printf("%f %f %f || %f %f %f || %f %f %f || %f %f %f \r\n", feetPosX[0],feetPosY[0],feetPosZ[0], feetPosX[1],feetPosY[1],feetPosZ[1], feetPosX[2],feetPosY[2],feetPosZ[2], feetPosX[3],feetPosY[3],feetPosZ[3]);
}//end IK init


void kinematika_kaki(int sel, float X, float Z, float Y){

		newPosX[sel]=feetPosX[sel]+X;
		newPosZ[sel]=feetPosZ[sel]+Z;
		newPosY[sel]=feetPosY[sel]+Y;

		switch (sel) {
			case 0:
				transformX=(newPosX[sel]*cos(45*PI/180))-(newPosZ[sel]*sin(45*PI/180));
				transformZ= newPosX[sel]*sin(45*PI/180) +newPosZ[sel]*cos(45*PI/180);
				break;
			case 1:
				transformX=(newPosX[sel]*cos(135*PI/180))-(newPosZ[sel]*sin(135*PI/180));
				transformZ= newPosX[sel]*sin(135*PI/180)+newPosZ[sel]*cos(135*PI/180);
				break;
			case 2:
				transformX=(newPosX[sel]*cos(225*PI/180))-(newPosZ[sel]*sin(225*PI/180));
				transformZ= newPosX[sel]*sin(225*PI/180)+newPosZ[sel]*cos(225*PI/180);
				break;
			case 3:
				transformX=(newPosX[sel]*cos(315*PI/180))-(newPosZ[sel]*sin(315*PI/180));
				transformZ= newPosX[sel]*sin(315*PI/180)+newPosZ[sel]*cos(315*PI/180);
				break;
			default:
				break;
		}//end leg select

			transformY	=	newPosY[sel];
      coxaFeetDistance	=	(sqrt(sq(transformX)+sq(transformZ)));
	              IKSW	=	sqrt(sq(coxaFeetDistance-coxaLength)+sq(transformY));
                  IKA1	=	atan((coxaFeetDistance-coxaLength)/transformY);
	              IKA2	=	acos((sq(tibiaLength)-sq(femurLength)-sq(IKSW))/(-2*IKSW*femurLength));
	            Tangle	=	acos((sq(IKSW)-sq(femurLength)-sq(tibiaLength))/(-2*tibiaLength*femurLength));

	   tibiaAngle[sel]	=	90-Tangle*180/PI; //beta
	   femurAngle[sel]	=	(IKA1+IKA2)*180/PI-90; //alpha
	    coxaAngle[sel]	=	90-atan2(transformZ,transformX)*180/PI; //gamma


}//end kinematika kaki

void servoCtrl(){
//	kakia((coxaAngle[0]*10)+(1740-430),(-femurAngle[0]*10)+1600,(-tibiaAngle[0]*10)+1240);
	kakia((coxaAngle[0]*10)+(1740-430),(-femurAngle[0]*10)+1530,(-tibiaAngle[0]*10)+1140);
	kakid((coxaAngle[3]*10)+(1355+430),(femurAngle[3]*10)+1850,(tibiaAngle[3]*10)+1700);
	kakib((coxaAngle[1]*10)+(1130+430),(femurAngle[1]*10)+1310,(tibiaAngle[1]*10)+1520);
	kakic((coxaAngle[2]*10)+(1900-430),(-femurAngle[2]*10)+1580,(-tibiaAngle[2]*10)+1327);
}

//-----------------------------------------------------------------------------------START TIMER INTERRUPT
void KinematicsM(){//Interrupt for kinematics  #1

		if(stepstats==1){inc++;}else{inc=0;}

		for(int i=0; i<4;i++){

			//Transition
			if(posX[i]<pX[i]){pX[i]--;} else if(posX[i]>pX[i]){pX[i]++;} else{pX[i]=posX[i];}
			if(posZ[i]<pZ[i]){pZ[i]--;} else if(posZ[i]>pZ[i]){pZ[i]=posZ[i];} else{pZ[i]=posZ[i];}
			if(posY[i]<pY[i]){pY[i]=posY[i];} else if(posX[i]>pY[i]){pY[i]++;} else{pY[i]=posY[i];}

			//Calculate Kinematics
			kinematika_kaki(i,pX[i],pZ[i],pY[i]);

		}
		if(servo==1){servoCtrl();}

}//end KinematicsM

void msTimer(){//Timer Interrupt 10ms
	penandaBuzzer++;
	if(penandaBuzzer<10){buzz=1;}else{buzz=0;}
}//end mstimer

void move(float Z, float X, float Y){

	stepstats=true;
	if (inc>speed){step++; inc=0; stepstats=0;}
	if (step>3){step=0; langkah++;}

	switch (step) {
		case 0:
			//Transfer
			posY[0]=-Y; posY[2]=-Y;
			posZ[0]=Z-10; posZ[2]=Z-10;
			posX[0]=X; posX[2]=X;
			//Support
			posY[1]=0; posY[3]=0;
			posZ[1]=-Z+(-10); posZ[3]=-Z+(-10);
			posX[1]=-X; posX[3]=-X;
			break;
		case 1:
			//Transisi
			posY[0]=0; posY[1]=0; posY[2]=0; posY[3]=0;
			break;
		case 2:
			//Support
			posY[0]=0; posY[2]=0;
			posZ[0]=-Z+(-10); posZ[2]=-Z+(-10);
			posX[0]=-X; posX[2]=-X;
			//Transfer
			posY[1]=-Y; posY[3]=-Y;
			posZ[1]=Z-10; posZ[3]=Z-10;
			posX[1]=X; posX[3]=X;
			break;
		case 3:
			//Transisi
			posY[0]=0; posY[1]=0; posY[2]=0; posY[3]=0;
			break;
		default:
			break;
	}
}//end move

void tripod_gait(float Z, float X, float Y){
	stepstats=true;
	if (inc>speed){step++; inc=0; stepstats=0;}
	if (step>7){step=0;  langkah++;}

	switch (step) {
		case 0:
			//Transfer
			posY[1]=-Y;
			posZ[1]=Z;
			//Support
			posY[0]=0; posY[2]=0; posY[3]=0;
			posZ[0]=-Z; posZ[2]=-Z/2; posZ[3]=Z/2;
			break;
		case 1:
			//transisi
			posY[0]=0; posY[1]=0; posY[2]=0; posY[3]=0;
			break;
		case 2:
			//Transfer
			posY[0]=-Y;
			posZ[0]=Z;
			//Support
			posY[1]=0; posY[2]=0; posY[3]=0;
			posZ[1]=Z/2; posZ[2]=-Z; posZ[3]=-Z/2;
			break;
		case 3:
			//Transisi
			posY[0]=0; posY[1]=0; posY[2]=0; posY[3]=0;
			break;
		case 4:
			//Transfer
			posY[2]=-Y;
			posZ[2]=Z;
			//Support
			posY[0]=0; posY[1]=0; posY[3]=0;
			posZ[0]=Z/2; posZ[1]=-Z/2; posZ[3]=-Z;
			break;
		case 5:
			//Transisi
			posY[0]=0; posY[1]=0; posY[2]=0; posY[3]=0;
			break;
		case 6:
			//Transfer
			posY[3]=-Y;
			posZ[3]=Z;
			//Support
			posY[0]=0; posY[1]=0; posY[2]=0;
			posZ[0]=-Z/2; posZ[1]=-Z; posZ[2]=Z/2;
			break;
		case 7:
			//Transisi
			posY[0]=0; posY[1]=0; posY[2]=0; posY[3]=0;
			break;
		default:
			break;
	}
}//end tripod gait

int main(void){

		IKinit();									//initiate kinematics
		Kinematics.attach(&KinematicsM,0.0015);		//Initiate timer kinematics
		msInterrupt.attach(&msTimer,0.01);			//Initiate 10 milisecond timer
		penandaBuzzer=0;
		servo=true;

		//ait(8);
		penandaBuzzer=0;
		while(1){
/*


			posZ[0]=40; wait(1);
			posZ[0]=30; wait(1);
			posZ[0]=20; wait(1);
			posZ[0]=10; wait(1);
			posZ[0]=0; wait(1);
			posZ[0]=-10; wait(1);
			posZ[0]=-20; wait(1);
			posZ[0]=-30; wait(1);
			posZ[0]=-40; wait(1);
*/

			move(40,0,15);
			/*int AS=0;
			if(usBtn==0){AS=1;}
			while(AS==1){move(40,0,15);}
			*/
			//dualpod
/*
			//move(40,0,15);
			posY[1]=5; posZ[2]=posY[3]=2; posY[0]=5;
			posZ[1]=20; posZ[3]=20;
			posX[1]=-20; posX[3]=30;

			while(langkah<=5){

			}
			while(1){
				for(int y; y<4; y++){
					posX[y]=0;
					posZ[y]=0;
					posY[y]=0;
				}
			}


			posY[0]=20; wait(10);
			posY[0]=10; wait(10);
			posY[0]=0; wait(10);
			posY[0]=-10; wait(10);
			posY[0]=-20; wait(10);


			posZ[0]=0; wait(10);
			posZ[0]=10; wait(10);
			posZ[0]=20; wait(10);
			posZ[0]=30; wait(10);
			posZ[0]=40; wait(10);
						posZ[0]=-10; wait(10);
						posZ[0]=-20; wait(10);
						posZ[0]=-30; wait(10);
						posZ[0]=-40; wait(10);
			//tripod_gait(40,0,15);
			posX[0]=0; wait(10);
			posX[0]=10; wait(10);
			posX[0]=20; wait(10);
			posX[0]=30; wait(10);
			posX[0]=40; wait(10);
						posX[0]=-10; wait(10);
						posX[0]=-20; wait(10);
						posX[0]=-30; wait(10);
						posX[0]=-40; wait(10);

			for(int u=0; u<2200; u++){
				//kakia((coxaAngle[0]*10)+(1740-430),(-femurAngle[0]*10)+1600,(-tibiaAngle[0]*10)+1240);
				kakia((coxaAngle[0]*10)+(1740-430),(-femurAngle[0]*10)+1600,u);
				wait_ms(1);
			}

			for(int u=2200; u>0; u--){
				//kakia((coxaAngle[0]*10)+(1740-430),(-femurAngle[0]*10)+1600,(-tibiaAngle[0]*10)+1240);
				kakia((coxaAngle[0]*10)+(1740-430),(-femurAngle[0]*10)+1600,u);
				wait_ms(1);
			}


*/

				/*kakid((coxaAngle[3]*10)+(1355+430),(femurAngle[3]*10)+1850,(tibiaAngle[3]*10)+1700);
				kakib((coxaAngle[1]*10)+(1130+430),(femurAngle[1]*10)+1310,(tibiaAngle[1]*10)+1520);
				kakic((coxaAngle[2]*10)+(1900-430),(-femurAngle[2]*10)+1580,(-tibiaAngle[2]*10)+1327);*/
		}//end while 1

}//END main








